angular
    .module('altairApp')
    .controller('usuariosCtrl',
        function($compile, $scope,$timeout, DTOptionsBuilder, DTColumnBuilder, $http, $window) {
            var vm = this;    
                vm.dt_data = [];               
                vm.item = {};
                vm.edit = edit;
                vm.dtOptions = DTOptionsBuilder.newOptions()
                    .withOption('initComplete', function() {
                        $timeout(function() {
                            $compile($('.dt-uikit .md-input'))($scope);
                        })
                    })
                    .withPaginationType('full_numbers')
                    .withOption('createdRow', function (row, data, dataIndex) {
                        $compile(angular.element(row).contents())($scope);
                    })
                    .withOption('ajax', {
                        dataSrc: function(json) {
                            json['draw']=1
                            json['recordsFiltered'] = json.records.length                            
                            json['recordsTotal'] =json.records.length
                            return json.records;
                          },
                        url: '../ws/usuarios',
                        type: 'GET',
                    })
                    .withOption('processing', true)
                    .withOption('responsive', true);

                    var index = 1;
                    countIndex = function () {
                        return index++;
                    }
                   
                vm.dtColumns = [
                  DTColumnBuilder.newColumn(countIndex).withTitle('Id'),
                  DTColumnBuilder.newColumn('nombre').withTitle('Nombre'),                
                  DTColumnBuilder.newColumn('usuario').withTitle('Usuario'),                
                  DTColumnBuilder.newColumn('tipos_usuarios.tipousuario').withTitle('Tipo Usuario'),
                  DTColumnBuilder.newColumn('correo').withTitle('Correo'),
                  DTColumnBuilder.newColumn('created_at').withTitle('Creado'),
                  DTColumnBuilder.newColumn(null).withTitle('Acciones').notSortable().renderWith(function(data,type,full){                  
                  vm.item[data.id] = data;
                  return  ' <a href="#" data-uk-modal="{target:\'#modalEditar\'}" ng-click="CtrlUsuario.edit(CtrlUsuario.item[' + data.id + '])">'+
                          ' <i class="md-icon material-icons md-bg-light-blue-900 uk-text-contrast"></i></a>'+
                          ' <a href="#" data-uk-modal="{target:\'#modalEliminar\'}" ng-click="CtrlUsuario.edit(CtrlUsuario.item[' + data.id + '])">'+
                          ' <i class="md-icon material-icons md-bg-red-900 uk-text-contrast">&#xE872;</i></a>';
                  })                    
              ];
        function edit(item){
               $scope.item = item;
               console.log(item);
               console.log(JSON.stringify(item));   
            }

        var modalCrear      = UIkit.modal("#modalCrear");
        var modalEditar     = UIkit.modal("#modalEditar");
        var modalEliminar   = UIkit.modal("#modalEliminar");

      //Validar Campos
        var $formValidate = $('#form_validation');

            $formValidate
                .parsley()
                .on('form:validated',function() {
                    $scope.$apply();
                })
                .on('field:validated',function(parsleyField) {
                    if($(parsleyField.$element).hasClass('md-input')) {
                        $scope.$apply();
                    }
                });
           

            $scope.ObjTipoUsuario = {};
            $scope.tipousuario = [];

            $http.get('../ws/tiposusuarios').success(function(datas)
            {
                angular.forEach(datas.records, function(value, key)
                {   
                    $scope.tipousuario.push({
                        id:         value.id,
                        title:      value.tipousuario,
                        value:      value.id,
                        parent_id:  value.id 
                    });
                });
            });

            $scope.ObjTipoUsuario['tipousuario'] = $scope.tipousuario;

            $scope.confTipoUsuario = {
                create: false,
                maxItems: 1,
                placeholder: 'Tipo de Usuario',
                optgroupField: 'parent_id',
                optgroupLabelField: 'title',
                optgroupValueField: 'ogid',
                valueField: 'value',
                labelField: 'title',
                searchField: 'title'
            };

        //Crear Usuario
            $scope.crearUsuario = function(item)
            {
                $scope.item = item;
                $scope.idtipousuario  = parseInt(item.idtipousuario);

    
                $http({
                    method: "POST",
                    url: "../ws/usuarios",
                    data: {
                        nombre:         item.nombre,
                        apellido:       item.apellido,
                        usuario:        item.usuario,
                        password:       item.password,
                        idtipousuario:  $scope.idtipousuario,
                        correo:         item.correo
                    }
                }).then(function mySucess(response)
                {
                    console.log(response.data.result);

                    if (response.data.result == false) 
                    {
                        $scope.notificacion = "<a href='#' class='notify-action'>Undo</a>"+response.data.message;
                        $timeout(function(){angular.element('#Notify').triggerHandler('click');}, 0);
                    }
                    else
                    {
                        modalCrear.hide();
                        //swal('Correcto!','Usuario creado con exito!','success');
                        //$window.location.reload();
                    }
                    
                },function myError(response){
                    console.log("Error");
                });
            }

    })//Fin del Controlador
