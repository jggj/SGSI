<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    protected $table = 'usuarios';
    protected $fillable = ['nombre', 'apellido', 'usuario','idtipousuario','correo'];
    protected $hidden = ['password'];

    //Relaciones
    public function TiposUsuarios()
    {
        return $this->hasOne('App\TiposUsuarios','id','idtipousuario');
    }
}
