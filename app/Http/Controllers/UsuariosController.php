<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use Request;
use Illuminate\Support\Facades\Validator;
use App\User;

class UsuariosController extends Controller
{
    public $message = "";
    public $result	= false;
    public $records	= array();

    public function index()
    {
    	try 
    	{
    		$registros 		= User::with('TiposUsuarios')->get();
    		$statusCode 	= 200;

    		$this->records 	= $registros;
    		$this->message 	= "Consulta Exitosa";
    		$this->result 	= true;	
    	} 
    	catch (\Exception $e) 
    	{
    		$statusCode 	= 404;
    		$this->message 	= "No existen registros";
    		$this->result 	= false;    		
    	}
    	finally
    	{
    		$response =
    		[
    			'message'	=> $this->message,
    			'result'	=> $this->result,
    			'records'	=> $this->records
    		];
    			return response()->json($response, $statusCode);
    	}
    }

   /* public function store(Request $request)
    {
    	try 
    	{
    		$nuevoRegistro = \DB::transaction(function() use ($request)
    		{
    			$nuevoRegistro = User::create([    				
    		            'nombre'		=> $request->input('nombre'),
    					'apellido'		=> $request->input('apellido'),
    					'usuario' 		=> $request->input('usuario'),
                        'idtipousuario' => $request->input('idtipousuario'),
    					'correo'	    => $request->input('correo'),
    					'password'		=> \Hash::make($request->input('password'))
    				]);

    			if (! $nuevoRegistro) 
    			{
    				throw new \Exception("El registro no se creo");
    			}
    			else
    			{
    				return $nuevoRegistro;
    			}
    		});
    			$statusCode 	= 200;
    			$this->records 	= $nuevoRegistro;
    			$this->message 	= "El registro se creo correctamente";
    			$this->result 	= true;
    	} 
    	catch (\Exception $e) 
    	{
    		$statusCode 	= 200;
    		$this->message 	= env('APP_DEBUG')?$e->getMessage(): "El registro no se creo";
    	}
    	finally
    	{
    		$response =
    		[
    			'message' 	=> $this->message,
    			'result'	=> $this->result,
    			'records'	=> $this->records
    		];
    			return response()->json($response, $statusCode);
    	}
    }*/

    public function store()
    {
        try 
        {
            $data =  Request::all();
            $rules = array(
                    'nombre'    => 'required', 
                    'apellido'  => 'required', 
                    'usuario'   => 'required',
                    'idtipousuario' => 'required',
                    'correo'    => 'required'
                );

           $v = validator::make($data, $rules);

           if ($v->fails()){
                    throw new \Exception("Hay campos vacios");              
           }
           

            User::create($data);

            $statusCode     = 200;
            $this->records  = $data;
            $this->message  = "El registro se creo correctamente";
            $this->result   = true;
        } 
        catch (\Exception $e) 
        {
            $statusCode     = 200;
            $this->message  = env('APP_DEBUG')?$e->getMessage(): "El registro no se creo";
        }
        finally
        {
            $response =
            [
                'message'   => $this->message,
                'result'    => $this->result,
                'records'   => $this->records
            ];
                return response()->json($response, $statusCode);

        }
    }

    public function update(Request $request, $id)
    {
        try 
        {
            $editarRegistro = \DB::transaction(function() use ($request, $id)
            {
                $registro = User::find($id);
                if (! $registro) 
                {
                  throw new \Exception("No existe el registro");
                        
                }
                else
                {
                    $registro->nombre           = $request->input('nombre', $registro->nombre);
                    $registro->apellido         = $request->input('apellido', $registro->apellido);
                    $registro->usuario          = $request->input('usuario', $registro->usuario);
                    $registro->idtipousuario    = $request->input('idtipousuario', $registro->idtipousuario);
                    $registro->correo           = $request->input('correo', $registro->correo);
                    if ($request->has('password') || $request->input('password') !="")
                        $registro->password = \Hash::make($request->input('password'));
                    $registro->save();
                    return $registro;
                }
            });
                $statusCode     = 200;
                $this->records  = $editarRegistro;
                $this->message  = "Se edito correctamente el registro";
                $this->result   = true;
        } 
        catch (\Exception $e) 
        {
          $statusCode       = 200;
          $this->message    = env('APP_DEBUG')?$e->getMessage():"El registro no se edito";
          $this->result     = false;   
        }
        finally
        {
            $response =
            [
                'message'   => $this->message,
                'result'    => $this->result,
                'records'   => $this->records
            ];
                return response()->json($response, $statusCode);
        }
    }

     public function destroy($id)
    {
       try 
        {
            $eliminarRegistro = \DB::transaction(function() use($id)
            {
                $registro = User::find($id);
                if (!$registro) 
                {
                    throw new \Exception("El registro no existe");
                }
                else
                {
                    $registro->delete();
                }
            });
                $statusCode     = 200;
                $this->message  = "El registro se elimino correctamente";
                $this->result   = true;
        } 
        catch (\Exception $e) 
        {
            $statusCode     = 200;
            $this->message  = "El registro se elimino correctamente";
            $this-> result  = true;
        }
        finally
        {
            $response =
            [
                'message' => $this->message,
                'result'  => $this->result,
            ];
                return response()->json($response, $statusCode);
        }
    }
}
