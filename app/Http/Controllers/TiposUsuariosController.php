<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TiposUsuarios;
class TiposUsuariosController extends Controller
{
    public $message = "";
    public $result	= false;
    public $records	= array();

    public function index()
    {
    	try 
    	{
    		$registros 		= TiposUsuarios::all();
    		$statusCode 	= 200;

    		$this->records 	= $registros;
    		$this->message 	= "Consulta Exitosa";
    		$this->result 	= true;	
    	} 
    	catch (\Exception $e) 
    	{
    		$statusCode 	= 404;
    		$this->message 	= "No existen registros";
    		$this->result 	= false;    		
    	}
    	finally
    	{
    		$response =
    		[
    			'message'	=> $this->message,
    			'result'	=> $this->result,
    			'records'	=> $this->records
    		];
    			return response()->json($response, $statusCode);
    	}
    }

    public function store(Request $request)
    {
    	try 
    	{
    		$nuevoRegistro = \DB::transaction(function() use ($request)
    		{
    			$nuevoRegistro = TiposUsuarios::create([    				
    					'tipousuario'		=> $request->input('tipousuario')
    				]);

    			if (! $nuevoRegistro) 
    			{
    				throw new \Exception("El registro no se creo");
    			}
    			else
    			{
    				return $nuevoRegistro;
    			}
    		});
    			$statusCode 	= 200;
    			$this->records 	= $nuevoRegistro;
    			$this->message 	= "El registro se creo correctamente";
    			$this->result 	= true;
    	} 
    	catch (\Exception $e) 
    	{
    		$statusCode 	= 200;
    		$this->message 	= env('APP_DEBUG')?$e->getMessage(): "El registro no se creo";
    	}
    	finally
    	{
    		$response =
    		[
    			'message' 	=> $this->message,
    			'result'	=> $this->result,
    			'records'	=> $this->records
    		];
    			return response()->json($response, $statusCode);
    	}
    }

    public function update(Request $request, $id)
    {
        try 
        {
            $editarRegistro = \DB::transaction(function() use ($request, $id)
            {
                $registro = TiposUsuarios::find($id);
                if (! $registro) 
                {
                  throw new \Exception("No existe el registro");
                        
                }
                else
                {
                    $registro->tipousuario = $request->input('tipousuario', $registro->tipousuario);
                    $registro->save();
                    return $registro;
                }
            });
                $statusCode     = 200;
                $this->records  = $editarRegistro;
                $this->message  = "Se edito correctamente el registro";
                $this->result   = true;
        } 
        catch (\Exception $e) 
        {
          $statusCode       = 200;
          $this->message    = env('APP_DEBUG')?$e->getMessage():"El registro no se edito";
          $this->result     = false;   
        }
        finally
        {
            $response =
            [
                'message'   => $this->message,
                'result'    => $this->result,
                'records'   => $this->records
            ];
                return response()->json($response, $statusCode);
        }
    }

     public function destroy($id)
    {
       try 
        {
            $eliminarRegistro = \DB::transaction(function() use($id)
            {
                $registro = TiposUsuarios::find($id);
                if (!$registro) 
                {
                    throw new \Exception("El registro no existe");
                }
                else
                {
                    $registro->delete();
                }
            });
                $statusCode     = 200;
                $this->message  = "El registro se elimino correctamente";
                $this->result   = true;
        } 
        catch (\Exception $e) 
        {
            $statusCode     = 200;
            $this->message  = "El registro se elimino correctamente";
            $this-> result  = true;
        }
        finally
        {
            $response =
            [
                'message' => $this->message,
                'result'  => $this->result,
            ];
                return response()->json($response, $statusCode);
        }
    }
}
